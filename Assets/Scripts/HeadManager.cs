﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadManager : MonoBehaviour
{
    public GameObject closedMouth;
    public GameObject openMouth;
    public SuctionManager suction;

    public bool isOpen = false;

    // Start is called before the first frame update
    void Start()
    {
        openMouth.SetActive(false);
        closedMouth.SetActive(true);
        suction.SuctionOff();
        StartCoroutine(SwitchRoutine());
    }

    public void Open()
    {
        isOpen = true;
    }

    IEnumerator SwitchRoutine()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            if (isOpen)
            {
                openMouth.SetActive(true);
                closedMouth.SetActive(false);
                suction.SuctionOn();
                yield return new WaitForSeconds(10);
                isOpen = false;
                openMouth.SetActive(false);
                closedMouth.SetActive(true);
                suction.SuctionOff();
            }
        }
    }
}
