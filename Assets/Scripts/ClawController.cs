﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClawController : MonoBehaviour
{

    [HideInInspector]
    public Vector3 movement;
    [HideInInspector]
    public Vector3 velocity;
    protected Vector3 targetVelocity;
    protected Vector3 velocitySmoothing;

    public GameObject rotationPivot;

    public float speed = 4f;
    public float accelerationTime = 1;

    public float accelerationTimeModifier = 1;
    public float accelerationTimeStoppedModifier = 2;


    public float rotationSpeed = 4f;
    private int rotation = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void Reset()
    {
        movement = new Vector3(0, 0, 0);
        rotation = 0;
    }

    public void MoveForward()
    {
        movement.x = 1;
    }

    public void MoveBackward()
    {
        movement.x = -1;
    }

    public void MoveLeft()
    {
        movement.z = 1;
    }

    public void MoveRight()
    {
        movement.z = -1;
    }

    public void MoveUp()
    {
        movement.y = 1;
    }

    public void MoveDown()
    {
        movement.y = -1;
    }

    public void RotateLeft()
    {
        rotation = -1;
    }

    public void RotateRight()
    {
        rotation = 1;
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        targetVelocity = movement * speed;

        rotationPivot.transform.Rotate(0, rotation * rotationSpeed * Time.deltaTime, 0, Space.Self);

        if (targetVelocity.x == 0)
        {
            velocity.x = Mathf.SmoothDamp(velocity.x, 0, ref velocitySmoothing.x, accelerationTime, speed, accelerationTimeStoppedModifier * Time.deltaTime);
        }
        else
        {
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocity.x, ref velocitySmoothing.x, accelerationTime, speed, accelerationTimeModifier * Time.deltaTime);
        }

        if (targetVelocity.y == 0)
        {
            velocity.y = Mathf.SmoothDamp(velocity.y, 0, ref velocitySmoothing.y, accelerationTime, speed, accelerationTimeStoppedModifier * Time.deltaTime);
        }
        else
        {
            velocity.y = Mathf.SmoothDamp(velocity.y, targetVelocity.y, ref velocitySmoothing.y, accelerationTime, speed, accelerationTimeModifier * Time.deltaTime);
        }

        if (targetVelocity.z == 0)
        {
            velocity.z = Mathf.SmoothDamp(velocity.z, 0, ref velocitySmoothing.z, accelerationTime, speed, accelerationTimeStoppedModifier * Time.deltaTime);
        }
        else
        {
            velocity.z = Mathf.SmoothDamp(velocity.z, targetVelocity.z, ref velocitySmoothing.z, accelerationTime, speed, accelerationTimeModifier * Time.deltaTime);
        }

        transform.Translate(velocity * Time.deltaTime);
        Vector3 pos = transform.position;

        pos.x = Mathf.Clamp(transform.position.x, -20f, 20f);
        pos.y = Mathf.Clamp(transform.position.y, 1.5f, 25f);
        pos.z = Mathf.Clamp(transform.position.z, -10f, 45f);

        transform.position = pos;
    }
}
