﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LauncherTest : MonoBehaviour
{

    public float force = 10000;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate(){

        foreach(Collider collider in Physics.OverlapBox(transform.position+transform.up, transform.localScale, transform.rotation)){

            Vector3 forceDirection = transform.up;
            var rigidBody = collider.GetComponent<Rigidbody>();
            if(rigidBody){

                rigidBody.AddForce(forceDirection.normalized*force*Time.fixedDeltaTime);
            }
        }
    }
}
