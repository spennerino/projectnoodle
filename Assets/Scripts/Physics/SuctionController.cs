﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuctionController : MonoBehaviour
{
    public float pullRadius = 2;
    public float pullForce = 1;

    public void FixedUpdate()
    {
        foreach (Collider collider in Physics.OverlapSphere(transform.position, pullRadius))
        {
            //Debug.Log(collider.transform.name);
            // calculate direction from target to me
            Vector3 forceDirection = transform.position - collider.transform.position;
            var rigidBody = collider.GetComponent<Rigidbody>();
            if (rigidBody)
            {
                // apply force on target towards me
                collider.GetComponent<Rigidbody>().AddForce(forceDirection.normalized * pullForce * Time.fixedDeltaTime);
            }
        }
    }
}
