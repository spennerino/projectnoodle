﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClawArmController : MonoBehaviour
{
    // Start is called before the first frame update
    float speed = 90.0f;
    [SerializeField]
    float maxAngle = 90;
    [SerializeField]
    float minAngle = 0;
    float angle = 0;

    public void Open()
    {
        if (angle <= maxAngle)
        {
            transform.parent.Rotate(0, 0, -speed * Time.deltaTime, Space.Self);
            angle += speed * Time.deltaTime;
        }
    }

    public void Close()
    {
        if (angle >= minAngle)
        {
            transform.parent.Rotate(0, 0, speed * Time.deltaTime, Space.Self);
            angle -= speed * Time.deltaTime;
        }
    }
}
