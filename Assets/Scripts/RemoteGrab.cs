﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RemoteGrab : MonoBehaviour
{
    public bool isHoldingRemote = false;
    public PhoneGrab phone;
    public float throwForce = 10f;
    public ButtonLabeler ButtonLabeler;
    private Camera cam;
    public GameObject grabParent;

    public List<ButtonOverlayLabeler> buttonOverlays;
    private Rigidbody rb;

    private MeshRenderer mr;
    public void OnButtonInteraction()
    {
        isHoldingRemote = !isHoldingRemote;
        if (isHoldingRemote)
        {
            if (phone.isHoldingPhone)
            {
                phone.OnButtonInteraction();
            }
            this.transform.SetParent(grabParent.transform);
            rb.constraints = RigidbodyConstraints.FreezeRotation;
            ButtonLabeler.isRenderOverridden = true;
            mr.enabled = false;
            foreach (var buttonOverlay in buttonOverlays)
            {
                buttonOverlay.isEnabled = true;
            }
        }
        else
        {
            Throw();
        }

    }

    private void Throw()
    {
        this.transform.SetParent(null);
        rb.constraints = RigidbodyConstraints.None;
        rb.velocity = Vector3.zero;
        rb.AddForce(throwForce * cam.transform.forward, ForceMode.Impulse);

        ButtonLabeler.isRenderOverridden = false;
        foreach (var buttonOverlay in buttonOverlays)
        {
            buttonOverlay.isEnabled = false;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        mr = ButtonLabeler.GetComponent<MeshRenderer>();
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (isHoldingRemote)
        {
            this.transform.localPosition = new Vector3(-0.5f, -0.1f, 1f);
            this.transform.localRotation = new Quaternion(-1.15f, -2.16f, 0.04f, 1.58f);
        }

        if (transform.position.y < -0.5f)
        {
            Vector3 position = transform.position;
            position.y = 25;
            transform.position = position;
        }
    }
}
