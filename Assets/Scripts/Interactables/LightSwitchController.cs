﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LightSwitchController : MonoBehaviour
{
    public void Switch()
    {
        RenderSettings.ambientLight = Color.black;
        RenderSettings.sun.color = Color.black;
        StartCoroutine(Reload());
    }

    void Update()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }

    IEnumerator Reload()
    {
        yield return new WaitForSeconds(2);
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
