﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoodleSpawner : MonoBehaviour
{
    private bool isSpawn;
    public SoundManager soundManager;

    int index;
    public List<GameObject> noodles;

    public void Spawn()
    {
        isSpawn = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnRoutine());
    }

    // Update is called once per frame
    IEnumerator SpawnRoutine()
    {
        while (true)
        {
            if (isSpawn)
            {
                Instantiate(noodles[index], transform.position, transform.rotation);
                if (index == 1)
                {
                    soundManager.Extrude.Play();
                    yield return new WaitForSeconds(2);
                    soundManager.Extrude.Pause();
                }
                else if (index == 2)
                {
                    soundManager.Extrude.Play();
                    yield return new WaitForSeconds(8);
                    soundManager.Extrude.Pause();
                }
                else
                {
                    soundManager.Extrude.Play();
                    yield return new WaitForSeconds(4);
                    soundManager.Extrude.Pause();
                }
                isSpawn = false;
                index++;
                if (index >= noodles.Count)
                {
                    index = 0;
                }
            }
            else
            {
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
