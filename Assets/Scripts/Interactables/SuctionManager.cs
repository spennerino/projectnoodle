﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class SuctionManager : MonoBehaviour
{
    private Camera cam;
    public List<SuctionController> suctions;
    public SoundManager soundManager;
    private float timer = 0;
    private float waitTime = 1f;
    private bool isSlurp = false;

    void Awake()
    {
        cam = Camera.main;
        StartCoroutine(PlaySlurp());
    }

    public void SuctionOn()
    {
        soundManager.Suction.Play();
        foreach (var suction in suctions)
        {
            suction.enabled = true;
        }
    }

    public void SuctionOff()
    {
        soundManager.Suction.Pause();
        foreach (var suction in suctions)
        {
            suction.enabled = false;
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        Debug.Log(collider.gameObject.name);
        if (collider.gameObject.name == "Player")
        {
            cam.transform.SetParent(null);
        }
        var rigidBody = collider.GetComponent<Rigidbody>();
        if (rigidBody)
        {
            Destroy(collider.gameObject);
        }

        if (!isSlurp)
        {
            soundManager.Slurp.Play();
        }

        isSlurp = true;
        timer = 0;
        print("reset");
    }

    IEnumerator PlaySlurp()
    {
        print("yay");
        while (true)
        {
            if (timer > waitTime)
            {
                soundManager.Slurp.Stop();
                print("stop");
            }

            print(timer);

            yield return new WaitForEndOfFrame();
            timer += Time.deltaTime;
        }
    }
}