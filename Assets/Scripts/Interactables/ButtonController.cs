﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
    public GameObject button;
    public Vector3 positionOffset;
    private Vector3 originalPosition;
    // Start is called before the first frame update 

    void Awake()
    {
        originalPosition = button.transform.localPosition;
    }

    public void OnHold()
    {
        button.transform.localPosition = positionOffset + originalPosition;
    }

    public void OnRelease()
    {
        button.transform.localPosition = originalPosition;
    }
}
