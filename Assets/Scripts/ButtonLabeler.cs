﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonLabeler : MonoBehaviour
{
    //public TextMesh button;
    //public string buttontext;
    Camera cam;
    Vector3 pos;

    public bool isRenderOverridden;

    public float distance = 3;
    public UnityEvent action;
    public UnityEvent actionHold;
    public UnityEvent actionRelease;

    private TextMesh textMesh;
    private MeshRenderer renderer;

    // Start is called before the first frame update
    void Awake()
    {
        //button = gameObject.AddComponent<TextMesh>();
        //button = new TextMesh();
        cam = Camera.main;
        transform.position = transform.position;
        isRenderOverridden = false;
        //GetComponent<TextMesh>().text = buttontext;
        renderer = GetComponent<MeshRenderer>();
        textMesh = GetComponent<TextMesh>();
    }
    
    // Update is called once per frame
    void Update()
    {
        float d = Vector3.Distance(transform.position, cam.transform.position);
        if (d < distance)
        {
            if (!isRenderOverridden)
            {
                renderer.enabled = true;
            }
            if (d < distance && Input.GetKeyDown(textMesh.text.ToLower()))
            {
                action.Invoke();
            }

            if (d < distance && Input.GetKey(textMesh.text.ToLower()))
            {
                actionHold.Invoke();
            }
            else {
                actionRelease.Invoke();
            }
        }
        else
        {
            renderer.enabled = false;
        }
        transform.position = transform.position;
        transform.rotation = cam.transform.rotation;
    }
}
