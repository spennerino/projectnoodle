﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonOverlayLabeler : MonoBehaviour
{
    public UnityEvent action;
    public UnityEvent actionHold;
    public UnityEvent actionRelease;

    private TextMesh textMesh;
    private MeshRenderer renderer;
    public bool isEnabled = false;

    // Start is called before the first frame update
    void Start()
    {
        textMesh = GetComponent<TextMesh>();
        renderer = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isEnabled)
        {
            renderer.enabled = true;

            if (Input.GetKeyDown(textMesh.text.ToLower()))
            {
                action.Invoke();
            }

            if (Input.GetKey(textMesh.text.ToLower()))
            {
                actionHold.Invoke();
            }
            else if(Input.GetKeyUp(textMesh.text.ToLower()))
            {
                actionRelease.Invoke();
            }
        }
        else
        {
            renderer.enabled = false;
        }
    }
}
