﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PhoneGrab : MonoBehaviour
{
    public UnityEvent callMade;
    public HeadManager head;
    public RemoteGrab remote;
    public float throwForce = 15f;
    public Quaternion q;
    public bool isHoldingPhone = false;
    public ButtonLabeler ButtonLabeler;
    public Microphone microphone;
    private AudioClip micInput;
    public float micSensitivity = 0.6f;
    private float[] micData;
    private Camera cam;
    private Rigidbody rb;
    private MeshRenderer mr;
    public void OnButtonInteraction()
    {
        isHoldingPhone = !isHoldingPhone;
        if (isHoldingPhone)
        {
            if (remote.isHoldingRemote)
            {
                remote.OnButtonInteraction();
            }
            this.transform.SetParent(cam.transform);
            rb.constraints = RigidbodyConstraints.FreezeRotation;
            ButtonLabeler.isRenderOverridden = true;
            mr.enabled = false;
        }
        else
        {
            Throw();
        }

    }

    private void Throw()
    {
        this.transform.SetParent(null);
        rb.constraints = RigidbodyConstraints.None;
        rb.velocity = Vector3.zero;
        rb.AddForce(throwForce * cam.transform.forward, ForceMode.Impulse);
        ButtonLabeler.isRenderOverridden = false;
    }

    // Start is called before the first frame update
    void Awake()
    {
        if (Microphone.devices.Length > 0)
        {
            micInput = Microphone.Start(Microphone.devices[0], true, 999, 44100);
            print("mic working");
        }
        cam = Camera.main;
        rb = GetComponent<Rigidbody>();
        mr = ButtonLabeler.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //get mic volume
        int dec = 128;
        float[] waveData = new float[dec];
        int micPosition = Microphone.GetPosition(null) - (dec + 1); // null means the first microphone
        micInput.GetData(waveData, micPosition);

        // Getting a peak on the last 128 samples
        float levelMax = 0;
        for (int i = 0; i < dec; i++)
        {
            float wavePeak = waveData[i] * waveData[i];
            if (levelMax < wavePeak)
            {
                levelMax = wavePeak;
            }
        }
        float level = Mathf.Sqrt(Mathf.Sqrt(levelMax));

        if (isHoldingPhone)
        {
            this.transform.localPosition = new Vector3(-0.7f, -0.1f, 1f);
            this.transform.localRotation = new Quaternion(-0.85f, 0.36f, -0.68f, -1f);
            
            if (level > micSensitivity)
            {
                if (!head.isOpen)
                {
                    callMade.Invoke();
                }
                head.Open();
            }
        }

        if (transform.position.y < -0.5f)
        {
            Vector3 position = transform.position;
            position.y = 25;
            transform.position = position;
        }
    }
}
