﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveJunior : MonoBehaviour
{
    public float distanceToRaise = 0;
    float speedX;
    float speedY;
    float speedZ;
    Vector3 finalPosition;
    Vector3 startPosition;

    public float dampingRatio;
    public float angularFrequency;

    // Start is called before the first frame update
    void Awake()
    {
        finalPosition = transform.localPosition;
        startPosition = transform.localPosition;
        startPosition.y -= distanceToRaise;
        transform.localPosition = startPosition;
        StartCoroutine(Raise());
    }

    public IEnumerator Raise()
    {
        yield return new WaitForSeconds(30);
        Vector3 position = transform.localPosition;
        while (Mathf.Abs(position.y - finalPosition.y) > 0.1f)
        {
            Spring(ref position.x, ref speedX, finalPosition.x, dampingRatio, angularFrequency, Time.deltaTime);
            Spring(ref position.y, ref speedY, finalPosition.y, dampingRatio, angularFrequency, Time.deltaTime);
            Spring(ref position.z, ref speedZ, finalPosition.z, dampingRatio, angularFrequency, Time.deltaTime);
            transform.localPosition = position;
            yield return new WaitForEndOfFrame();
        }

        transform.localPosition = finalPosition;
    }

    public static void Spring(ref float x, ref float v, float xt, float zeta, float omega, float h)
    {
        float f = 1.0f + 2.0f * h * zeta * omega;
        float oo = omega * omega;
        float hoo = h * oo;
        float hhoo = h * hoo;
        float detInv = 1.0f / (f + hhoo);
        float detX = f * x + h * v + hhoo * xt;
        float detV = v + hoo * (xt - x);
        x = detX * detInv;
        v = detV * detInv;
    }
}
