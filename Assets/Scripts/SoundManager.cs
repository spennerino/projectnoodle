﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource ButtonPush;
    public AudioSource Claw;
    public AudioSource Extrude;
    public AudioSource ItsReady;
    public AudioSource LightSwitch;
    public AudioSource Slurp;
    public AudioSource Suction;

    private int numClawButtonsHeld = 0;
    public void PlayButtonPush() 
    {
        ButtonPush.Play();
    }

    public void StartClaw()
    {
        numClawButtonsHeld += 1;
        if (!Claw.isPlaying)
        {
            Claw.Play();
        }
    }
    public void StopClaw()
    {
        numClawButtonsHeld -= 1;
        if (Claw.isPlaying && numClawButtonsHeld == 0)
        {
            Claw.Pause();
        }
    }

    public void PlayExtrude(int seconds)
    {
        Extrusion(seconds);
    }

    private IEnumerator Extrusion(int n)
    {
        Extrude.Play();
        yield return new WaitForSeconds(n);
        Extrude.Stop();
    }

    public void PlayItsReady()
    {
        ItsReady.Play();
    }

    public void PlayLightSwitch()
    {
        LightSwitch.Play();
    }

    public void PlaySlurp()
    {
        Slurp.Play();
    }

    public void PlaySuction()
    {
        Suction.Play();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
